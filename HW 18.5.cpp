#include <iostream>
#include <string>
#include <stdexcept>

using namespace std;

template<typename T>
class STACK
{
public:
    STACK() : root(nullptr) {} // �� ��������� ������ ����

    void push(const T& x)

    {
        Node* newnode = new Node;
        newnode->data = x;
        newnode->next = root;
        root = newnode;
    }
    bool empty() const
    {
        return root == nullptr; // 
    }
    /*const T& top()const
    {
		if (empty())
		{
			throw length_error("stack is empty");
		}
        return root->data;
    }*/
    T pop()
    {
        if (empty())
        {
            throw length_error("stack is empty");
        }

        Node * delnode = root;
        T x = delnode->data;
        root = delnode->next;
        delete delnode;
        return x;
    }
	//void Print()
	//{
	//	T* p; // ��������� ���������, ��������� �� ��������� �����

	//	// 1. ���������� ��������� p �� ������� �����
	//	p = STACK;

	//	// 2. ����
	//	cout << "STACK: " << endl;
	//	if (root == 0)
	//		cout << "is empty." << endl;

	//	for (int i = 0; i < root; i++)
	//	{
	//		cout << "Item[" << i << "] = " << *p << endl;
	//		p++; // ���������� ��������� �� ��������� �������
	//	}
	//	cout << endl;
	//}

    ~STACK()
    {
		while (!empty())
		{
			pop();
		}
    }
private:
    struct Node
    {
        T data; // ���� � �������
        Node * next; // ��������� �� ��������� 
    };
    Node* root; //������� ����� 

};

int main()
{
    cout << "Enter" << '\n';
    int y;
    y = 0;
    cin >> y;

    STACK<float> st;

    for (int i = 1; i <= y; i++)
    {
        st.push(i);
    }

	cout << "Enter" << '\n';
	int z;
	z = 0;
	cin >> z;

	for (int i = 1; i <= z; i++)
	{
		st.push(i);
	}

    while (!st.empty())
    {
        cout << st.pop() << "_ _" << '\n';
    }

	return 0;
}
